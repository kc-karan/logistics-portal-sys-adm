const db = require('../config/conn');
const to = require('../utils/to');

let exp = {};

exp.login = async (req, res) => {
    try {
        let username = String(req.body.username).trim();
        let password = String(req.body.password).trim();
        let category = String(req.body.category).trim(); // undefined if admin chosen
        let err, result;
        let sql = 'SELECT * FROM users WHERE username = ? AND password = ?';
        
        [err, result] = await to(db.query(sql, [ username, password ]));

        if(err) {
            return res.sendError(null);
        } else { // result is not null
            
            if(result.length <= 0) {
                return res.sendError(null, 'User not found');
            } else {
                let data = result[0];
                console.log(data);
                if(data.admin === 1) { // admin
                   req.session.isLoggedIn = true;
                   req.session.isadmin = true;
                   req.session.admin_email = data.email;
                   req.session.admin_username = data.username;
                   req.session.admin_id = data.user_id;
                   req.session.save();
                   return res.redirect('/admin'); 
                } else {
                    req.session.isLoggedIn = true;
                    req.session.isadmin = false;
                    req.session.cat_email = data.email;
                    req.session.cat_username = data.username;
                    req.session.cat_id = data.user_id;
                    req.session.category = data.category;
                    req.session.save();
                    return res.redirect('/category'); 
                }
            }
        }


    } catch(err) {
        console.log(err);
		return res.sendError(null);
    }
}

exp.logout = async (req, res) => {
	try {
        req.session.destroy((err) => {
			if (err) {
                console.log(err);
            }
			return res.redirect('/');
		});
	} catch (err) {
		console.log(err);
		return res.redirect('/');
	}
}

module.exports = exp;