const express = require('express');
var router = express.Router();
const path = require('path');
const auth = require('./auth');

function loggedIn(access) {
    return (req, res, next) => {
        if(typeof(req.session.isLoggedIn) === 'undefined' || req.session.isLoggedIn === false) {
            return res.redirect('/');
        } else if(req.session.type == access) { // sahi hai
            return next();
        } else {
            return res.redirect('/');
        }
    }
}

function notLoggedIn(req, res, next) {
    if (typeof(req.session.logged_in) === 'undefined' || req.session.logged_in === false) {
        return next();
    } else {
        return res.redirect('/');
    }
}

router.get('/', notLoggedIn, (req, res) => {
    res.render('index', {});
});

router.post('/form', notLoggedIn, auth.login);


router.get('/logout', auth.logout);

module.exports = router;
